#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import math
def calkulater():
    w=1
    while(w==1):
        try:
            text = str(input("Введите операцию которую хотите выполнить(+-плюс --минус *-умножить /-деление //-деление на цело %-остаток от деления **-степень артан-артангенс гипотенуза-гипотинуза треугольника кописигн-кописигн логарифм-логарифм площадь-площадь периметр-периметр): "))
            if(text=="0"):
                break;
            elif (text != "площадь" and text != "периметр"):
                a = float(input("Введите число: "))
                b = float(input("Введите число: "))
            
            else:
                x = float(input("Введите число: "))
                y = float(input("Введите число: "))
                z = float(input("Введите число: "))
            if(text == "+"):
                print(a+b)
            elif(text == "-"):
                print(a-b)
            elif(text == "*"):
                print(a*b)
            elif(text == "/"):
                print(a/b)
            elif(text == "//"):
                print(a//b)
            elif(text == "%"):
                print(a%b)
            elif(text == "**"):
                print(a**b)
            elif(text == "артан"):
                print(math.atan2(a,b))
            elif(text == "гипотенуза"):
                print(math.hypot(a,b))
            elif(text == "кописигн"):
                print(math.copysign(a,b))
            elif(text == "логарифм"):
                print(math.log(a,b))
            elif(text == "площадь"):
                l = lambda x,y,z: 2*(x*y+y*z+x*z)
                print(l(x,y,z))
            elif(text == "периметр"):
                l = lambda x,y,z: 4*x+4*y+4*z
                print(l(x,y,z))
        except:
            print("Ошибка")
def chislo():
    try:
        a=str(input("Введите строку "))
        prubel = 0
        bukv = 0
        zap = 0
        for i in a:
            if(i==' '):
                prubel=prubel+1
            elif(i==','):
                zap=zap+1
            else:
                bukv=bukv+1
        print("букв:",bukv)
        print("пробелов:",prubel)
        print("запятых:",zap)
    except:
        print("Ошибка") 
def matrix():
    try:
        col = int(input("кол-во столбцов в матрице "))
        row = int(input("кол-во строк в матрице "))
        start = int(input("начало, т. е. с какого значения должно начинаться матрица "))
        nex = int(input(" шаг, т. е. на какое число должно увеличиваться последующее значение матрицы "))
     
        mat=[]
        for i in range(row):
            mat.append([])
            for j in range(col):
                mat[i].append(start)
                start+=nex;
        for i in mat: 
            print(' '.join(list(map(str, i))))
    except:
        print("Ошибка")
        
try:
    w=1
    while(w==1):
        a = int(input("Введите номер функции(0 чтобы выйте): "))
        if(a==1):
            calkulater()
        elif(a==2):
            chislo()
        elif(a==3):
            matrix()
        else:
            break;
except:
    print("Ошибка")


# In[6]:


import cv2

cam = cv2.VideoCapture(0)
while True:
    ret, img = cam.read()
    cv2.imshow('camera', img)
    if cv2.waitKey(10)==27:
        break
cam.release()
cv2.destroyAllWindows()


# In[ ]:


import pygame
import random

WIDTH = 360
HEIGHT = 480
FPS = 30

# Задаем цвета
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)

class Player(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface((50, 50))
        self.image.fill(GREEN)
        self.rect = self.image.get_rect()
        self.rect.center = (WIDTH / 2, HEIGHT / 2)
# Создаем игру и окно
pygame.init()
pygame.mixer.init()
screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("My Game")
score=0;
clock = pygame.time.Clock()
all_sprites = pygame.sprite.Group()
player = Player()
all_sprites.add(player)
font_name = pygame.font.match_font('arial')
def draw_text(surf, text, size, x, y):
    font = pygame.font.Font(font_name, size)
    text_surface = font.render(text, True, WHITE)
    text_rect = text_surface.get_rect()
    text_rect.midtop = (x, y)
    surf.blit(text_surface, text_rect)
# Цикл игры
running = True
while running:
    # Держим цикл на правильной скорости
    clock.tick(FPS)
    # Ввод процесса (события)
    for event in pygame.event.get():
        # check for closing window
        if event.type == pygame.QUIT:
            running = False

    # Обновление
        if event.type == pygame.MOUSEBUTTONDOWN:
            score+=1;
            
    # Рендеринг
    screen.fill(BLACK)
    draw_text(screen, str(score), 18, WIDTH / 2, 10)
    all_sprites.draw(screen)
    
    # После отрисовки всего, переворачиваем экран
    pygame.display.flip()

pygame.quit()


# In[ ]:




